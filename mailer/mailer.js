const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailerConfig;
if (process.env.NODE_ENV === 'development') {
    mailerConfig = {
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: process.env.ETHEREAL_USER,
            pass: process.env.ETHEREAL_PASS
        }
    };
}
else {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_KEY
        }
    }
    mailerConfig = sgTransport(options);
}

module.exports = nodemailer.createTransport(mailerConfig);