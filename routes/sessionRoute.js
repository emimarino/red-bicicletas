const passport = require('../config/passport');
var express = require('express');
var router = express.Router();
var controller = require('../controllers/sessionController');

router.get('/login', function (req, res) {
    res.render('session/login')
});

router.post('/login', function (req, res, next) {
    passport.authenticate('local', function (err, usuario, info) {
        if (err) {
            return next(err);
        }
        if (!usuario) {
            return res.render('session/login', { info });
        }

        req.logIn(usuario, function (err) {
            if (err) {
                return next(err);
            }

            return res.redirect('/');
        });
    })(req, res, next);
});

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/login')
});

router.get('/confirmation/:token', controller.confirmation_get);
router.post('/confirmation/:token', controller.confirmation_post);
router.get('/forgotPassword', controller.forgotPassword_get);
router.post('/forgotPassword', controller.forgotPassword_post);
router.get('/resetPassword/:token', controller.resetPassword_get);
router.post('/resetPassword/:token', controller.resetPassword_post);

module.exports = router;