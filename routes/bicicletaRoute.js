var express = require('express');
var router = express.Router();
var controller = require('../controllers/bicicletaController');

router.get('/', controller.index);
router.get('/create', controller.create_get);
router.post('/create', controller.create_post);
router.post('/delete/:id', controller.delete_post);
router.get('/update/:id', controller.update_get);
router.post('/update/:id', controller.update_post);
router.get('/detail/:id', controller.detail);

module.exports = router;