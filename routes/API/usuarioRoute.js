var express = require('express');
var router = express.Router();
var controller = require('../../controllers/API/usuarioController');

router.get('/', controller.index);
router.post('/create', controller.create);
router.post('/reservar', controller.reservar);

module.exports = router;