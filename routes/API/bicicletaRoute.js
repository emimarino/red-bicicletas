var express = require('express');
var router = express.Router();
var controller = require('../../controllers/API/bicicletaController');

router.get('/', controller.index);
router.get('/detail/:id', controller.detail);
router.post('/create', controller.create);
router.post('/delete/:id', controller.delete);
router.post('/update/:id', controller.update);

module.exports = router;