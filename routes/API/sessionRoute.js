const passport = require('../../config/passport');
var express = require('express');
var router = express.Router();
var controller = require('../../controllers/API/sessionController');

router.post('/authenticate', controller.authenticate);
router.post('/forgotPassword', controller.forgotPassword);
router.post('/facebook_token', passport.authenticate('facebook-token'), controller.authFacebookToken);

module.exports = router;