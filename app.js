require('newrelic');
require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const mongoDbStore = require('connect-mongodb-session')(session);

let store;
if (process.env.NODE_ENV === 'development') {
    store = new session.MemoryStore;
}
else {
    store = new mongoDbStore({
        uri: process.env.MONGO_URI,
        collection: 'sessions'
    });
    store.on('error', function (error) {
        assert.ifError(error);
        assert.ok(false);
    });
}

var indexRouter = require('./routes/indexRoute');
var usersRouter = require('./routes/usersRoute');
var bicicletaRouter = require('./routes/bicicletaRoute');
var usuarioRouter = require('./routes/usuarioRoute');
var sessionRouter = require('./routes/sessionRoute');
var sessionAPIRouter = require('./routes/API/sessionRoute');
var bicicletaAPIRouter = require('./routes/API/bicicletaRoute');
var usuarioAPIRouter = require('./routes/API/usuarioRoute');
var authenticationModel = require('./models/authenticationModel');

var app = express();
app.set('secretKey', 'jwt_key_password');

app.use(session({
    cookie: { maxAge: 240 * 60 * 60 * 1000 },
    store: store,
    saveUninitialized: true,
    resave: 'true',
    secret: '{909C698F-B0FB-415A-B7D5-9688452BFE57}'
}));

var mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', sessionRouter);
app.use('/', indexRouter);
app.use('/users', authenticationModel.loggedIn, usersRouter);
app.use('/bicicleta', authenticationModel.loggedIn, bicicletaRouter);
app.use('/usuario', authenticationModel.loggedIn, usuarioRouter);
app.use('/api/session', sessionAPIRouter);
app.use('/api/bicicleta', authenticationModel.validateJWT, bicicletaAPIRouter);
app.use('/api/usuario', authenticationModel.validateJWT, usuarioAPIRouter);
app.use('/privacy_policy', function (req, res) { res.sendFile('public/privacy_policy.html', { root: __dirname }); });
app.use('/google3534cdff43987d98', function (req, res) { res.sendFile('public/google3534cdff43987d98.html', { root: __dirname }); });

app.get('/auth/google', passport.authenticate('google', { scope: ['email', 'profile'] }));
app.get('/auth/google/callback', passport.authenticate('google', { successRedirect: '/', failureRedirect: '/error' }));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
