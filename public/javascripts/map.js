var mymap = L.map('map').setView([-34.9504613,-57.9420298], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1
}).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicleta",
    success: function(result){
        result.bicicletas.forEach(function(bicicleta){
            L.marker(bicicleta.ubicacion, {title: bicicleta.id}).addTo(mymap);
        });
    }
});