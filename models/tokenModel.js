var mongoose = require('mongoose');

var tokenSchema = new mongoose.Schema({
    _userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Usuario'
    },
    token: {
        type: String,
        required: true
    },
    creationDate: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 43200
    }
});

tokenSchema.statics.getByToken = function (token, callback) {
    return this.findOne({ token: token }, callback);
}

module.exports = mongoose.model('Token', tokenSchema);