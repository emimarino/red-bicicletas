const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const reservaModel = require('./reservaModel');
const tokenModel = require('./tokenModel');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const saltRounds = 10;
const mailer = require('../mailer/mailer');
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const validateEmail = function (email) {
    return emailRegex.test(email);
}

const requiredPassword = function () {
    return this.verificado == true;
}

var usuarioSchema = new mongoose.Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'El email no es valido'],
        match: [emailRegex]
    },
    password: {
        type: String,
        required: [requiredPassword, 'El password es obligatorio'],
        default: ''
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario' });

usuarioSchema.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.toString = function () {
    return `nombre: ${this.nombre} | email: ${this.email}`;
}

usuarioSchema.statics.getAll = function (callback) {
    return this.find({}, callback);
}

usuarioSchema.statics.createInstance = function (nombre, email) {
    return new this({
        nombre,
        email
    });
}

usuarioSchema.statics.add = function (usuario, callback) {
    return this.create(usuario, callback);
}

usuarioSchema.statics.getById = function (id, callback) {
    return this.findOne({ _id: id }, callback);
}

usuarioSchema.statics.deleteById = function (id, callback) {
    return this.deleteOne({ _id: id }, callback);
}

usuarioSchema.statics.update = function (id, nombre, email, callback) {
    return this.findOneAndUpdate({ _id: id }, { nombre: nombre, email: email }, { returnNewDocument: true, runValidators: true, context: 'query' }, callback); // alternative use findByIdAndUpdate
}

usuarioSchema.statics.findOrCreateByGoogle = function (googleProfile, callback) {
    console.log(googleProfile);
    const self = this;
    self.findOne({
        $or: [{ 'googleId': googleProfile.id }, { 'email': googleProfile.emails[0].value }]
    }, (err, result) => {
        if (result) {
            return callback(err, result);
        }
        else {
            let values = {
                googleId: googleProfile.id,
                email: googleProfile.emails[0].value,
                nombre: googleProfile.displayName || 'SIN NOMBRE',
                verificado: true,
                password: crypto.randomBytes(16).toString('hex')
            };
            self.create(values, (err, result) => {
                if (err) {
                    return console.log(err.message);
                }
                return callback(err, result);
            });
        }
    });
}

usuarioSchema.statics.findOrCreateByFacebook = function (facebookProfile, callback) {
    console.log(facebookProfile);
    const self = this;
    self.findOne({
        $or: [{ 'facebookId': facebookProfile.id }, { 'email': facebookProfile.emails[0].value }]
    }, (err, result) => {
        if (result) {
            return callback(err, result);
        }
        else {
            let values = {
                facebookId: facebookProfile.id,
                email: facebookProfile.emails[0].value,
                nombre: facebookProfile.displayName || 'SIN NOMBRE',
                verificado: true,
                password: crypto.randomBytes(16).toString('hex')
            };
            self.create(values, (err, result) => {
                if (err) {
                    return console.log(err.message);
                }
                return callback(err, result);
            });
        }
    });
}

usuarioSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar = function (usuarioId, desde, hasta, callback) {
    var reserva = new reservaModel({ desde: desde, hasta: hasta, usuario: usuarioId, usuario: this._id });
    reserva.save(callback);
}

usuarioSchema.methods.sendVerificationEmail = function (callback) {
    const token = new tokenModel({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    const nombreDestination = this.nombre;
    const emailDestination = this.email;
    token.save(function (err) {
        if (err) {
            return console.log(err.message);
        }

        const mailOptions = {
            from: process.env.NODEMAILER_FROM,
            to: emailDestination,
            subject: 'Verificacion de cuenta',
            text: `Hola ${nombreDestination},\n\n Por favor verifique su cuenta haciendo click en el siguiente link: http://localhost:3000/confirmation/${token.token}\n`
        }

        mailer.sendMail(mailOptions, function (err) {
            if (err) {
                return console.log(err.message);
            }
        })
    });

    return token;
}

usuarioSchema.methods.sendForgotPasswordEmail = function (callback) {
    const nombreDestination = this.nombre;
    const emailDestination = this.email;
    const passwordResetToken = crypto.randomBytes(16).toString('hex');
    this.passwordResetToken = passwordResetToken;
    this.passwordResetTokenExpires = new Date(Date.now() + 24 * 60 * 60 * 1000);
    this.save(function (err) {
        if (err) {
            return console.log(err.message);
        }

        const mailOptions = {
            from: process.env.NODEMAILER_FROM,
            to: emailDestination,
            subject: 'Reestablecimiento de contrase\u00f1a',
            text: `Hola ${nombreDestination},\n\n Para restablecer su contrase\u00f1a haga click en el siguiente enlace: http://localhost:3000/resetPassword/${passwordResetToken}\n \n\n El enlace caducar\u00E1 despu\u00E9s de 24 horas.`
        }

        mailer.sendMail(mailOptions, function (err) {
            if (err) {
                return console.log(err.message);
            }
        })
    });

    return passwordResetToken;
}

module.exports = mongoose.model('Usuario', usuarioSchema);