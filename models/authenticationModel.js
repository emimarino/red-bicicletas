const jwt = require('jsonwebtoken');
var authentication = function () { }

authentication.loggedIn = function (req, res, next) {
    if (req.user) {
        next();
    }
    else {
        res.redirect('/login');
    }
}

authentication.validateJWT = function (req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
        if (err) {
            res.json({ status: "error", message: err.message, data: null });
        }
        else {
            req.body.userId = decoded.id;
            console.log('jwt decoded: ' + decoded)
            next();
        }
    });
}

module.exports = authentication;