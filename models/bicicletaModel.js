var mongoose = require('mongoose');

var bicicletaSchema = new mongoose.Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: { type: '2Dsphere', sparse: true }
    }
});

bicicletaSchema.methods.toString = function () {
    return `id: ${this.id} | color: ${this.color} | modelo: ${this.modelo} | ubicacion: ${this.ubicacion}`;
}

bicicletaSchema.statics.getAll = function (callback) {
    return this.find({}, callback);
}

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this({
        code,
        color,
        modelo,
        ubicacion
    });
}

bicicletaSchema.statics.add = function (bicicleta, callback) {
    return this.create(bicicleta, callback);
}

bicicletaSchema.statics.getById = function (id, callback) {
    return this.findOne({ code: id }, callback);
}

bicicletaSchema.statics.deleteById = function (id, callback) {
    return this.deleteOne({ code: id }, callback);
}

bicicletaSchema.statics.update = function (id, color, modelo, ubicacion, callback) {
    return this.findOneAndUpdate({ code: id }, { color: color, modelo: modelo, ubicacion: ubicacion }, { new: true }, callback); // alternative use findByIdAndUpdate
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);