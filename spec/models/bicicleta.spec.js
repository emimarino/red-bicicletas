var mongoose = require('mongoose');
const modelBicicleta = require('../../models/bicicletaModel');

describe('Testing Bicicletas', function () {
    beforeEach(function (done) {
        var mongodb = 'mongodb://localhost/red_bicicletas_unit_test';
        mongoose.createConnection(mongodb, { useNewUrlParser: true, useUnifiedTopology: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error:'));
        db.once('open', function () {
            console.log('We are connected to test database!');
        });
        //jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        done();
    });

    afterEach(function (done) {
        modelBicicleta.deleteMany({}, function (err, success) {
            if (err) {
                console.log(err);
            }
            //mongoose.disconnect(err);
            done();
        });
    });

    describe('Bicicleta.create', () => {
        it('crear una bicicleta', () => {
            var bicicleta = modelBicicleta.createInstance(1, 'rojo', 'urbana', [-34.9504613, -57.9420298]);
            expect(bicicleta.code).toBe(1);
            expect(bicicleta.color).toBe('rojo');
            expect(bicicleta.modelo).toBe('urbana');
            expect(bicicleta.ubicacion[0]).toBe(-34.9504613);
            expect(bicicleta.ubicacion[1]).toBe(-57.9420298);
        });
    });

    describe('Bicicleta.getAll', () => {
        it('comienza vacia', function (done) {
            modelBicicleta.getAll(function (err, bicicletas) {
                expect(bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agregar una bicicleta', (done) => {
            var newBicicleta = modelBicicleta.createInstance(1, 'rojo', 'urbana', [-34.9504613, -57.9420298]);
            modelBicicleta.add(newBicicleta, function (err, bicicleta) {
                if (err) {
                    console.log(err);
                }
                modelBicicleta.getAll(function (err, bicicletas) {
                    expect(bicicletas.length).toBe(1);
                    expect(bicicletas[0].code).toBe(newBicicleta.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.getById', () => {
        it('devolver bicicleta con id 1 y 2', function (done) {
            modelBicicleta.getAll(function (err, bicicletas) {
                expect(bicicletas.length).toBe(0);

                var newBicicleta1 = modelBicicleta.createInstance(1, 'rojo', 'urbana', [-34.9504613, -57.9420298]);
                modelBicicleta.add(newBicicleta1, function (err, bicicleta) {
                    if (err) {
                        console.log(err);
                    }

                    var newBicicleta2 = modelBicicleta.createInstance(2, 'blanco', 'paseo', [-34.948305, -57.9469689]);
                    modelBicicleta.add(newBicicleta2, function (err, bicicleta) {
                        if (err) {
                            console.log(err);
                        }
                        modelBicicleta.getById(1, function (err, bicicleta) {
                            expect(bicicleta.code).toBe(newBicicleta1.code);
                            expect(bicicleta.color).toBe(newBicicleta1.color);
                            expect(bicicleta.modelo).toBe(newBicicleta1.modelo);
                            expect(bicicleta.ubicacion[0]).toBe(newBicicleta1.ubicacion[0]);
                            expect(bicicleta.ubicacion[1]).toBe(newBicicleta1.ubicacion[1]);
                        });

                        modelBicicleta.getById(2, function (err, bicicleta) {
                            expect(bicicleta.code).toBe(newBicicleta2.code);
                            expect(bicicleta.color).toBe(newBicicleta2.color);
                            expect(bicicleta.modelo).toBe(newBicicleta2.modelo);
                            expect(bicicleta.ubicacion[0]).toBe(newBicicleta2.ubicacion[0]);
                            expect(bicicleta.ubicacion[1]).toBe(newBicicleta2.ubicacion[1]);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.deleteById', () => {
        it('eliminar bicicleta con id 1 y 2', function (done) {
            modelBicicleta.getAll(function (err, bicicletas) {
                expect(bicicletas.length).toBe(0);

                var newBicicleta1 = modelBicicleta.createInstance(1, 'rojo', 'urbana', [-34.9504613, -57.9420298]);
                modelBicicleta.add(newBicicleta1, function (err, bicicleta) {
                    if (err) {
                        console.log(err);
                    }

                    var newBicicleta2 = modelBicicleta.createInstance(2, 'blanco', 'paseo', [-34.948305, -57.9469689]);
                    modelBicicleta.add(newBicicleta2, function (err, bicicleta) {
                        if (err) {
                            console.log(err);
                        }

                        modelBicicleta.getAll(function (err, bicicletas) {
                            expect(bicicletas.length).toBe(2);
                            modelBicicleta.deleteById(1, function (err, bicicleta) {
                                modelBicicleta.getAll(function (err, bicicletas) {
                                    expect(bicicletas.length).toBe(1);
                                    modelBicicleta.deleteById(2, function (err, bicicleta) {
                                        modelBicicleta.getAll(function (err, bicicletas) {
                                            expect(bicicletas.length).toBe(0);
                                            done();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.update', () => {
        it('actualizar bicicleta con id 1', (done) => {
            var newBicicleta = modelBicicleta.createInstance(1, 'rojo', 'urbana', [-34.9504613, -57.9420298]);
            modelBicicleta.add(newBicicleta, function (err, bicicleta) {
                if (err) {
                    console.log(err);
                }
                modelBicicleta.update(1, 'rojo y blanco', 'paseo', [-34.948305, -57.9469689], function (err, bicicleta) {

                    modelBicicleta.getById(1, function (err, bicicleta) {
                        expect(bicicleta.code).toBe(1);
                        expect(bicicleta.color).toBe('rojo y blanco');
                        expect(bicicleta.modelo).toBe('paseo');
                        expect(bicicleta.ubicacion[0]).toBe(-34.948305);
                        expect(bicicleta.ubicacion[1]).toBe(-57.9469689);
                        done();
                    });
                });
            });
        });
    });
});