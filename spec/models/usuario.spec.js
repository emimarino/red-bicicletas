var mongoose = require('mongoose');
const modelBicicleta = require('../../models/bicicletaModel');
const modelUsuario = require('../../models/usuarioModel');
const modelReserva = require('../../models/reservaModel');

describe('Testing Usuarios', function () {
    beforeEach(function (done) {
        var mongodb = 'mongodb://localhost/red_bicicletas';
        mongoose.createConnection(mongodb, { useNewUrlParser: true, useUnifiedTopology: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error:'));
        db.once('open', function () {
            console.log('We are connected to test database!');
        });
        //jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        done();
    });

    afterEach(function (done) {
        modelReserva.deleteMany({}, function (err, success) {
            if (err) {
                console.log(err);
            }
            modelUsuario.deleteMany({}, function (err, success) {
                if (err) {
                    console.log(err);
                }
                modelBicicleta.deleteMany({}, function (err, success) {
                    if (err) {
                        console.log(err);
                    }
                    //mongoose.disconnect(err);
                    done();
                });
            });
        });
    });

    describe('Reservar Bicicletas', () => {
        it('debe existir la reserva', (done) => {
            const usuario = new modelUsuario({ nombre: 'Emiliano' });
            usuario.save();
            const bicicleta = new modelBicicleta({ code: 1, color: 'rojo', modelo: 'urbana' });
            bicicleta.save();

            var desde = new Date();
            var hasta = new Date();
            hasta.setDate(desde.getDate() + 1);
            usuario.reservar(bicicleta.id, desde, hasta, function (err, reserva) {
                modelReserva.find({}).populate('bicicleta').populate('usuario').exec(function (err, reserva) {
                    console.log(reserva[0]);
                    expect(reserva.length).toBe(1);
                    expect(reserva[0].diasDeReserva()).toBe(2);
                    expect(reserva[0].bicicleta.code).toBe(1);
                    expect(reserva[0].usuario.nombre).toBe('Emiliano');
                    done();
                });
            });
        });
    });
});