var mongoose = require('mongoose');
const modelBicicleta = require('../../models/bicicletaModel');
const request = require('request');
require('../../bin/www');

beforeEach(function (done) {
    var mongodb = 'mongodb://localhost/red_bicicletas_unit_test';
    mongoose.createConnection(mongodb, { useNewUrlParser: true, useUnifiedTopology: true });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));
    db.once('open', function () {
        console.log('We are connected to test database!');
    });
    //jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
    done();
});

afterEach(function (done) {
    modelBicicleta.deleteMany({}, function (err, success) {
        if (err) {
            console.log(err);
        }
        //mongoose.disconnect(err);
        done();
    });
});
const baseUrl = 'http://localhost:3000/api/bicicleta/';
describe('Bicicleta API', function () {
    describe('- GET', function () {
        it('List All', function (done) {
            request.get(baseUrl, function (error, response) {
                expect(response.statusCode).toEqual(200);
                console.log(response.statusCode);
                done();
            });
        });

        it('Detail', function (done) {
            var newBicicleta = modelBicicleta.createInstance(1, 'rojo', 'urbana', [-34.9504613, -57.9420298]);
            modelBicicleta.add(newBicicleta, function (err, bicicleta) {
                if (err) {
                    console.log(err);
                }
                request.get(baseUrl + 'detail/1', function (error, response) {
                    expect(response.statusCode).toEqual(200);
                    expect(JSON.parse(response.body).bicicleta.ubicacion).toEqual([-34.9504613, -57.9420298]);
                    done();
                });
            });
        });
    });

    describe('- POST', function () {
        it('Create', function (done) {
            request.post(baseUrl + 'create', {
                json: true,
                body: {
                    "id": 1,
                    "color": "color test",
                    "modelo": "modelo test",
                    "latitud": -35.9547577,
                    "longitud": -57.9540607
                }
            },
                function (error, response) {
                    expect(response.statusCode).toEqual(200);
                    modelBicicleta.getById(1, function (err, bicicleta) {
                        expect(bicicleta.color).toBe("color test");
                        expect(bicicleta.modelo).toBe("modelo test");
                        expect(bicicleta.ubicacion).toEqual([-35.9547577, -57.9540607]);
                        done();
                    });
                });
        });

        it('Delete', function (done) {
            var newBicicleta = modelBicicleta.createInstance(1, 'rojo', 'urbana', [-34.9504613, -57.9420298]);
            modelBicicleta.add(newBicicleta, function (err, bicicleta) {
                if (err) {
                    console.log(err);
                }
                request.post(baseUrl + 'delete/1', {
                    json: true,
                    body: {
                        "id": 1,
                        "color": "color test",
                        "modelo": "modelo test",
                        "latitud": -35.9547577,
                        "longitud": -57.9540607
                    }
                },
                    function (error, response) {
                        expect(response.statusCode).toEqual(200);
                        modelBicicleta.getAll(function (err, bicicletas) {
                            expect(bicicletas.length).toBe(0);
                            done();
                        });
                    });
            });
        });

        it('Update', function (done) {
            var newBicicleta = modelBicicleta.createInstance(1, 'rojo', 'urbana', [-34.9504613, -57.9420298]);
            modelBicicleta.add(newBicicleta, function (err, bicicleta) {
                if (err) {
                    console.log(err);
                }
                request.post(baseUrl + 'update/1', {
                    json: true,
                    body: {
                        "id": 1,
                        "color": "blanco",
                        "modelo": "paseo",
                        "latitud": -34.948305,
                        "longitud": -57.9469689
                    }
                },
                    function (error, response) {
                        expect(response.statusCode).toEqual(200);
                        modelBicicleta.getById(1, function (err, bicicleta) {
                            expect(bicicleta.ubicacion).toEqual([-34.948305, -57.9469689]);
                            done();
                        });
                    });
            });
        });
    });
});