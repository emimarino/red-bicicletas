const moment = require('moment');
const usuarioModel = require('../models/usuarioModel');
const tokenModel = require('../models/tokenModel');

exports.confirmation_get = function (req, res) {
    tokenModel.getByToken(req.params.token, function (err, token) {
        if (!token) {
            res.render('session/confirmation', { errors: { token: { message: 'El token no es valido' } }, model: new usuarioModel(), token: req.params.token });
            return;
        }

        usuarioModel.getById(token._userId, function (err, usuario) {
            if (!usuario) {
                res.render('session/confirmation', { errors: { token: { message: 'No existe un usuario para este token' } }, model: new usuarioModel(), token: req.params.token });
                return;
            }

            if (usuario.verificado) {
                res.render('session/confirmation', { errors: { token: { message: 'El usuario ya fue previamente confirmado' } }, model: usuario, token: req.params.token });
                return;
            }

            res.render('session/confirmation', { errors: {}, model: usuario, token: req.params.token });
        });
    });
}

exports.confirmation_post = function (req, res) {
    tokenModel.getByToken(req.params.token, function (err, token) {
        if (token) {
            usuarioModel.getById(token._userId, function (err, usuario) {
                if (usuario && !usuario.verificado) {
                    if (req.body.password != req.body.confirmPassword) {
                        usuario.password = req.body.password;
                        res.render('session/confirmation', { errors: { confirmPassword: { message: 'El confirmar password no coincide con el password ingresado' } }, model: usuario, token: req.params.token });
                        return;
                    }

                    usuario.verificado = true;
                    usuario.password = req.body.password;
                    usuario.save(function (err) {
                        if (err) {
                            res.render('session/confirmation', { errors: err.errors, model: usuario, token: req.params.token });
                        }
                        else {
                            res.redirect('/usuario#index');
                        }
                    });
                }
            });
        }
    });
}

exports.forgotPassword_get = function (req, res) {
    res.render('session/forgotPassword', { errors: {}, email: '', success: false });
}

exports.forgotPassword_post = function (req, res) {
    usuarioModel.findOne({ email: req.body.email }, function (err, usuario) {
        if (err || !usuario) {
            res.render('session/forgotPassword', { errors: { email: { message: 'Email no existente o incorrecto' } }, email: req.body.email, success: false });
            return;
        }

        if (!usuario.verificado) {
            usuario.sendVerificationEmail();
        }
        else {
            usuario.sendForgotPasswordEmail();
        }
        
        res.render('session/forgotPassword', { errors: {}, email: req.body.email, success: true });
    });
}

exports.resetPassword_get = function (req, res) {
    usuarioModel.findOne({ passwordResetToken: req.params.token }, function (err, usuario) {
        if (!usuario) {
            res.render('session/resetPassword', { errors: { token: { message: 'El token no es valido' } }, model: new usuarioModel(), token: req.params.token });
            return;
        }
        if (moment(Date.now()).isAfter(usuario.passwordResetTokenExpires)) {
            res.render('session/resetPassword', { errors: { token: { message: 'El token esta expirado' } }, model: usuario, token: req.params.token });
            return;
        }

        res.render('session/resetPassword', { errors: {}, model: usuario, token: req.params.token });
    });
}

exports.resetPassword_post = function (req, res) {
    usuarioModel.findOne({ passwordResetToken: req.params.token }, function (err, usuario) {
        if (usuario && moment(Date.now()).isBefore(usuario.passwordResetTokenExpires)) {
            if (req.body.password != req.body.confirmPassword) {
                usuario.password = req.body.password;
                res.render('session/resetPassword', { errors: { confirmPassword: { message: 'El confirmar password no coincide con el nuevo password ingresado' } }, model: usuario, token: req.params.token });
                return;
            }

            usuario.passwordResetToken = null;
            usuario.passwordResetTokenExpires = null;
            usuario.password = req.body.password;
            usuario.save(function (err) {
                if (err) {
                    res.render('session/resetPassword', { errors: err.errors, model: usuario, token: req.params.token });
                }
                else {
                    res.redirect('/login');
                }
            });
        }
    });
}