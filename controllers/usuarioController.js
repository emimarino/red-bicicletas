const usuarioModel = require('../models/usuarioModel');

exports.index = function (req, res) {
    usuarioModel.getAll(function (err, usuarios) {
        res.render('usuario/index', { model: usuarios });
    });
}

exports.create_get = function (req, res) {
    res.render('usuario/create', { errors: {}, model: new usuarioModel() });
}

exports.create_post = function (req, res) {
    var usuario = usuarioModel.createInstance(req.body.nombre, req.body.email)
    usuarioModel.add(usuario, function (err, usuario) {
        if (err) {
            res.render('usuario/create', { errors: err.errors, model: usuarioModel.createInstance(req.body.nombre, req.body.email) });
            return;
        }

        usuario.sendVerificationEmail();
        res.redirect('/usuario#index');
    });
}

exports.delete_post = function (req, res) {
    usuarioModel.deleteById(req.params.id, function (err, usuario) {
        res.redirect('/usuario#index');
    });
}

exports.update_get = function (req, res) {
    usuarioModel.getById(req.params.id, function (err, usuario) {
        res.render('usuario/update', { errors: {}, model: usuario, userId: req.params.id });
    });
}

exports.update_post = function (req, res) {
    usuarioModel.update(req.params.id, req.body.nombre, req.body.email, function (err, usuario) {
        if (err) {
            res.render('usuario/update', { errors: err.errors, model: usuarioModel.createInstance(req.body.nombre, req.body.email), userId: req.params.id });
            return;
        }

        res.redirect('/usuario#index');
    });
}

exports.detail = function (req, res) {
    usuarioModel.getById(req.params.id, function (err, usuario) {
        res.render('usuario/detail', { model: usuario });
    });
}