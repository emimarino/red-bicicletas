const bicicletaModel = require('../models/bicicletaModel');

exports.index = function (req, res) {
    bicicletaModel.getAll(function (err, bicicletas) {
        res.render('bicicleta/index', { model: bicicletas });
    });
}

exports.create_get = function (req, res) {
    res.render('bicicleta/create');
}

exports.create_post = function (req, res) {
    var newBicicleta = bicicletaModel.createInstance(req.body.id, req.body.color, req.body.modelo, [req.body.latitud, req.body.longitud]);
    bicicletaModel.add(newBicicleta, function (err, bicicleta) {
        res.redirect('/bicicleta#index');
    });
}

exports.delete_post = function (req, res) {
    bicicletaModel.deleteById(req.params.id, function (err, bicicleta) {
        res.redirect('/bicicleta#index');
    });
}

exports.update_get = function (req, res) {
    res.render('bicicleta/update', { model: bicicletaModel.getById(req.params.id) });
}

exports.update_post = function (req, res) {
    bicicletaModel.update(req.body.id, req.body.color, req.body.modelo, [req.body.latitud, req.body.longitud], function (err, bicicleta) {
        res.redirect('/bicicleta#index');
    });
}

exports.detail = function (req, res) {
    bicicletaModel.getById(req.params.id, function (err, bicicleta) {
        res.render('bicicleta/detail', { model: bicicleta });
    });
}