const jwt = require('jsonwebtoken');
const usuarioModel = require('../../models/usuarioModel');

exports.authenticate = function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        return res.status(401).json({ status: "error", message: "Por favor ingrese el email y el password", data: null });
    }
    else {
        usuarioModel.findOne({ email: req.body.email }, function (err, usuario) {
            if (err) {
                return next(err);
            }

            if (!usuario) {
                return res.status(401).json({ status: "error", message: "Email no existente o incorrecto", data: null });
            }
            if (!usuario.verificado) {
                return res.status(401).json({ status: "error", message: "Usuario no verificado", data: null });
            }
            if (!usuario.validPassword(req.body.password)) {
                return res.status(401).json({ status: "error", message: "Password incorrecto", data: null });
            }

            const token = jwt.sign({ id: usuario._id }, req.app.get('secretKey'), { expiresIn: '7d' });
            res.status(200).json({ message: "Usuario Encontrado", data: { usuario: usuario, token: token } });
        });
    }
}

exports.forgotPassword = function (req, res) {
    usuarioModel.findOne({ email: req.body.email }, function (err, usuario) {
        if (err) {
            return next(err);
        }

        if (!usuario) {
            return res.status(401).json({ status: "error", message: "Email no existente o incorrecto", data: null });
        }

        if (!usuario.verificado) {
            usuario.sendVerificationEmail();
        }
        else {
            usuario.sendForgotPasswordEmail();
        }

        return res.status(200).json({ message: "Se ha enviado un mail de reestablecimiento de contrase\u00f1a", data: null });
    });
}

exports.authFacebookToken = function (req, res, next) {
    if (req.user) {
        try {
            const token = jwt.sign({ id: req.user._id }, req.app.get('secretKey'), { expiresIn: '7d' });
            res.status(200).json({ message: "Usuario Encontrado", data: { usuario: req.user, token: token } });
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: err.message, user: req.user });
        }
    }
    else {
        res.status(401);
    }
}