const usuarioModel = require('../../models/usuarioModel');

exports.index = function (req, res) {
    usuarioModel.find({}, function (err, usuarios) {
        res.status(200).json({ usuarios: usuarios });
    });
}

exports.create = function (req, res) {
    var usuario = new usuarioModel({ nombre: req.body.nombre, email: req.body.email });
    usuario.save(function (err, usuario) {
        if (err) {
            return console.log(err.errors.email.message);
        }

        usuario.sendVerificationEmail(function (err, token) {
            if (err) {
                return console.log(err.message);
            }

            res.status(200).json({ token: token.populate('usuario') });
        });
    });
}

exports.reservar = function (req, res) {
    usuarioModel.findById(req.body.id, function (err, usuario) {
        usuario.reservar(req.body.bicicletaId, req.body.desde, req.body.hasta, function (err, reserva) {
            res.status(200).json({ reserva: reserva.populate('bicicleta').populate('usuario') });
        });
    });
}