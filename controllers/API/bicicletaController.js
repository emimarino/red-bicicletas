const bicicletaModel = require('../../models/bicicletaModel');

exports.index = function (req, res) {
    bicicletaModel.getAll(function (err, bicicletas) {
        res.status(200).json({ bicicletas: bicicletas });
    });
}

exports.create = function (req, res) {
    var newBicicleta = bicicletaModel.createInstance(req.body.id, req.body.color, req.body.modelo, [req.body.latitud, req.body.longitud]);
    bicicletaModel.add(newBicicleta, function (err, bicicleta) {
        res.status(200).json({ bicicleta: bicicleta });
    });
}

exports.delete = function (req, res) {
    bicicletaModel.deleteById(req.params.id, function (err, bicicleta) {
        bicicletaModel.getAll(function (err, bicicletas) {
            res.status(200).json({ bicicletas: bicicletas });
        });
    });
}

exports.update = function (req, res) {
    bicicletaModel.update(req.body.id, req.body.color, req.body.modelo, [req.body.latitud, req.body.longitud], function (err, bicicleta) {
        res.status(200).json({ bicicleta: bicicleta });
    });
}

exports.detail = function (req, res) {
    bicicletaModel.getById(req.params.id, function (err, bicicleta) {
        res.status(200).json({ bicicleta: bicicleta });
    });
}