const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const googleStrategy = require('passport-google-oauth2').Strategy;
const facebookStrategy = require('passport-facebook-token');
const usuarioModel = require('../models/usuarioModel');

passport.use(new localStrategy(function (email, password, done) {
    usuarioModel.findOne({ email: email }, function (err, usuario) {
        if (err) {
            return done(err);
        }
        if (!usuario) {
            return done(null, false, { message: 'Email no existente o incorrecto' });
        }
        if (!usuario.verificado) {
            return done(null, false, { message: 'Usuario no verificado' });
        }
        if (!usuario.validPassword(password)) {
            return done(null, false, { message: 'Password incorrecto' });
        }

        return done(null, usuario);
    });
}));

passport.use(new googleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST + "/auth/google/callback"
},
    function (request, accessToken, refreshToken, profile, done) {
        usuarioModel.findOrCreateByGoogle(profile, function (err, user) {
            return done(err, user);
        });
    }
));

passport.use(new facebookStrategy({
    clientID: process.env.FACEBOOK_CLIENT_ID,
    clientSecret: process.env.FACEBOOK_CLIENT_SECRET
},
    function (accessToken, refreshToken, profile, done) {
        usuarioModel.findOrCreateByFacebook(profile, function (err, user) {
            return done(err, user);
        });
    }
));

passport.serializeUser(function (user, callback) {
    callback(null, user.id);
});

passport.deserializeUser(function (id, callback) {
    usuarioModel.findById(id, function (err, usuario) {
        callback(err, usuario);
    });
});

module.exports = passport;